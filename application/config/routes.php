<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$route['default_controller'] = "welcome";
$route['customer'] = "customer/dashboard";
$route['customer/([a-z]+)'] = "customer/dashboard/$1"; 
$route['customer/ajax/([a-z]+)'] = "customer/ajax/$1";

$route['404_override'] = '';


/* End of file routes.php */ 
/* Location: ./application/config/routes.php */       