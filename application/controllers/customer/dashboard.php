﻿<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ALL | E_WARNING | E_NOTICE);
ini_set('display_errors', TRUE);


class Dashboard extends CI_Controller{


		  

        function index(){
			
			if (!$this->ion_auth->logged_in()){
			
				redirect('auth/login', 'refresh');
			
			}
			else{
			
				$this->load->model('customer/Customer_model'); 
				$data = $this->Customer_model->get_dashboard();
				echo $this->twig->render('customer/dashboard.twig', $data); 
				
			}
			
	    }





}