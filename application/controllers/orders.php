﻿<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller {




	function index()
	{
	
	  
		$this->list_orders();
	}
	
	
	function list_orders(){
	    
		
	   $this->load->model('Orders_model'); 
	   $dump = $this->Orders_model->list_orders();
	   $this->load->view('ordersview', $dump);
	
	}
	
	function add(){ 
	
	    $this->load->model('Orders_model');
	    header("Access-Control-Allow-Origin: *");
		
		
		
		function rand_chars($c, $l, $u = FALSE) {
						if (!$u) for ($s = '', $i = 0, $z = strlen($c)-1; $i < $l; $x = rand(0,$z), $s .= $c{$x}, $i++);
						else for ($i = 0, $z = strlen($c)-1, $s = $c{rand(0,$z)}, $i = 1; $i != $l; $x = rand(0,$z), $s .= $c{$x}, $s = ($s{$i} == $s{$i-1} ? substr($s,0,-1) : $s), $i=strlen($s));
						return $s;
		} 
		
		$orderid = rand_chars("WERTYUOPLKJHGFDSAZXCVBNM", 4).rand(1100, 119999);
	    
		
		
		$this->load->library('Mongo_db');
		date_default_timezone_set('Europe/Moscow');
		$date = date("F j, Y,");
		$time = date(" g:i a"); 
		
		$person = array(
			"order_id" => $orderid,
			"order_date" => $date,
			"order_time" => $time,
			"order_niche" => $_POST['niche'],
			"order_site_ref" => $_SERVER['HTTP_REFERER'],
			"order_name" => $_POST['name'],
			"order_phone" => $_POST['phone'],
			"order_status" => "new", 
			"order_cost" => "200",
			"order_buyer" => " ",
			"order_email" => " ",
			"order_comments" => " ",
);
		//$test = $this->mongo_db->delete_all('test');
		$test = $this->mongo_db->insert('test', $person);
		
	    //$this->Orders_model->Send_sms($person['order_name'],$person['order_phone']);

		$this->Orders_model->Send_email($person['order_niche'], $person['order_name'],$person['order_phone']);
	}
	
}
?>