<!DOCTYPE html>
<html>
<head>
<title>Dashboard</title>
<meta name="description" content="">
<meta charset="utf-8">



<!--[if lt IE 9]> 
   <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
<![endif]-->   

<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css">

</head>
<body>

<div class="navbar navbar-inverse">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#">www.chelny-offers.ru</a>
  </div>
  
  <div class="navbar-collapse collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Личный кабинет</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>  Мои покупки</a></li>
	   <li><a href="#"><span class="glyphicon glyphicon-list"></span>  История платежей</a></li>
    </ul>
    
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span>  Вход в систему</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-user"></span>  Регистрация</a></li>
    </ul>
	
  </div>
</div>
<?php $form_attr = array('class' => 'form-horizontal');
      $input_attr = array('class' => 'form-control');
	  $input_btn = array('type'=>'submit', 'name'=>'submit', 'value'=>' Вход в систему','class'=>'btn btn-success btn-xs');
 ?>
<div class="col-lg-4">
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title"><span class="glyphicon glyphicon-user"></span> Вход в систему</h3>
  </div>
  <div class="panel-body">
 
  
  <div id="infoMessage" class="alert alert-danger"><?php echo $message;?></div>

<?php echo form_open("auth/login", $form_attr);?>

  <p>
    <?php echo lang('login_identity_label', 'identity');?>
    <?php echo form_input($identity);?>
  </p>

  <p>
    <?php echo lang('login_password_label', 'password');?>
    <?php echo form_input($password);?>
  </p>

  <p>
    <?php echo lang('login_remember_label', 'remember');?>
    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
  </p>


  <p><?php echo form_submit($input_btn);?></p>

<?php echo form_close();?>

<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>

</div>
</div>
</div>


    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>js/theme.js"></script>
</body>
</html>